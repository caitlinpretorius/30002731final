/*
Title: Van Hire
Author: Caitlin
Date: 14/06/18
Purpose:  Capture the following type of information for van hire.
*/
//the name of the 'Van' class
class Van{
    get VanID(){
        return this._VanID;//"default" getter and setter for the "VanID" property

    }
    set VanID(value){
        this._VanID = value;
    }
    get Capacity(){
        return this._Capacity;//"default" getter and setter for the "Capacity" property

    }
    set Capacity(value){
        this._Capacity = value;
    }
    get License(){
        return this._License;//"default" getter and setter for the "License" property

    }
    set License(value){
        this._License = value;
    }
    get CostPerDay(){
        return this._CostPerDay;//"default" getter and setter for the "CostPerDay" property

    }
    set CostPerDay(value){
        this._CostPerDay = value;
    }
    get Insurance(){
        return this._Insurance;//"default" getter and setter for the "Insurance" property

    }
    set Insurance(value){
        this._Insurance = value;
    }
//Constructor for the "Van" Class
    constructor(VanID, Capacity, License, CostPerDay, Insurance){
        this._VanID = VanID;
        this._Capacity = Capacity;
        this._License = License;
        this._CostPerDay = CostPerDay;
        this._Insurance = Insurance;
    }
    totalCost(){//Method for the overall price of the cost per day and the insurance combined
        let overall = this._CostPerDay + this._Insurance
        return overall // returns the Total Hire Cost
    }
}

//Main Program
//Create a new object "Van1" using the "Van" class as a template
let Van1 = new Van
//Pass arguments to the class by prompting the user for input
Van1.VanID = prompt("EZ Van Hire: Enter Van Details.\n\nPlease enter your Van ID:")
Van1.Capacity = prompt("EZ Van Hire: Enter Van Details.\n\nPlease enter your Van Capacity(m3):")
Van1.License = prompt("EZ Van Hire: Enter Van Details.\n\nPlease enter your Van License:")
Van1.CostPerDay= Number(prompt("EZ Van Hire: Enter Van Details.\n\nPlease enter your Van Cost per day($):"))
Van1.Insurance = Number(prompt("EZ Van Hire: Enter Van Details.\n\nPlease enter your Van Insurance cost($):"))
//All information displayed in browser
console.log(`EZ Van Hire: Van Details`)//heading
console.log(`------------------------`)
console.log(`Van ID              : ${Van1.VanID}`)//the Van ID for "Van1"
console.log(`Load Capacity(m3)   : ${Van1.Capacity}`)//the Van Capacity for "Van1"
console.log(`License Type        : ${Van1.License}`)//the Van License for "Van1"
console.log(`Hire Cost Per Day   : $${Van1.CostPerDay.toFixed(2)}`)//the Van Cost Per Day for "Van1"
console.log(`Insurance Per Day   : $${Van1.Insurance.toFixed(2)}`)//the Van Insurance for "Van1"
console.log(`Total Hire Cost     : $${Van1.totalCost().toFixed(2)}`)////uses the "totalCost" method to display the Total Van Hire Cost for "Van1"
